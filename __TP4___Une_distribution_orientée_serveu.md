# TP4 : Une distribution orientée serveur

# Sommaire

- [TP4 : Une distribution orientée serveur](#tp4--une-distribution-orientée-serveur)
- [Sommaire](#sommaire)
- [I. Install de Rocky Linux](#i-install-de-rocky-linux)
- [II. Checklist](#ii-checklist)
- [III. Mettre en place un service](#iii-mettre-en-place-un-service)
  - [1. Intro NGINX](#1-intro-nginx)
  - [2. Install](#2-install)
  - [3. Analyse](#3-analyse)
  - [4. Visite du service web](#4-visite-du-service-web)
  - [5. Modif de la conf du serveur web](#5-modif-de-la-conf-du-serveur-web)

# I. Install de Rocky Linux


# II. Checklist

🌞 **Choisissez et définissez une IP à la VM**

```bash 
[marielise@localhost ~]$ sudo nano /etc/sysconfig/network-scripts/ifcfg-enp0s8
TYPE=Ethernet
BOOTPROTO=static
NAME=enp0s8
DEVICE=enp0s8
ONBOOT=yes
IPADDR=10.250.1.56
NETMASK=255.255.255.0
```
```bash
[marielise@localhost ~]$ ip a
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:d5:0c:d9 brd ff:ff:ff:ff:ff:ff
    inet 10.250.1.56/24 brd 10.250.1.255 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fed5:cd9/64 scope link 
       valid_lft forever preferred_lft forever
````

---

🌞 **Vous me prouverez que :**

- le service ssh est actif sur la VM

``` bash 
[marielise@localhost ~]$ sudo systemctl status sshd
● sshd.service - OpenSSH server daemon
   Loaded: loaded (/usr/lib/systemd/system/sshd.service; enabled; vendor preset>
   Active: active (running) since Tue 2021-11-23 16:48:25 CET; 4min 36s ago
     Docs: man:sshd(8)
           man:sshd_config(5)
 Main PID: 876 (sshd)
    Tasks: 1 (limit: 4945)
   Memory: 4.2M
   CGroup: /system.slice/sshd.service
           └─876 /usr/sbin/sshd -D -oCiphers=aes256-gcm@openssh.com,chacha20-po>

nov. 23 16:48:25 localhost.localdomain systemd[1]: Starting OpenSSH server daem>
nov. 23 16:48:25 localhost.localdomain sshd[876]: Server listening on 0.0.0.0 p>
nov. 23 16:48:25 localhost.localdomain sshd[876]: Server listening on :: port 2>
nov. 23 16:48:25 localhost.localdomain systemd[1]: Started OpenSSH server daemo>
nov. 23 16:49:25 localhost.localdomain sshd[1521]: Accepted password for mariel>
nov. 23 16:49:25 localhost.localdomain sshd[1521]: pam_unix(sshd:session): sess>
```

- vous pouvez vous connecter à la VM, grâce à un échange de clés

```bash 
[marielise@localhost .ssh]$ sudo nano authorized_keys
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCa7HbKnoU/8/7+MUfPkuW1eH7Ydgu/llpZyUo7xoE$
```
```bash 
MacBook-Pro-de-saperlipopette:.ssh marieliserenzema$ sudo nano id_rsa.pub
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCa7HbKnoU/8/7+MUfPkuW1eH7Ydgu/llpZyUo7xoE$
```
---

➜ **Accès internet**

🌞 **Prouvez que vous avez un accès internet**

- avec une commande `ping`

```bash
[marielise@localhost ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=63 time=26.3 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=63 time=25.5 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=63 time=28.4 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=63 time=29.8 ms
64 bytes from 8.8.8.8: icmp_seq=5 ttl=63 time=28.1 ms
^C
--- 8.8.8.8 ping statistics ---
5 packets transmitted, 5 received, 0% packet loss, time 4009ms
rtt min/avg/max/mdev = 25.518/27.630/29.757/1.528 ms
```



🌞 **Prouvez que vous avez de la résolution de nom**

```bash 
[marielise@localhost ~]$ ping ynov.com
PING ynov.com (92.243.16.143) 56(84) bytes of data.
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=1 ttl=63 time=25.6 ms
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=2 ttl=63 time=24.3 ms
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=3 ttl=63 time=23.7 ms
64 bytes from xvm-16-143.dc0.ghst.net (92.243.16.143): icmp_seq=4 ttl=63 time=23.6 ms
^C
--- ynov.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3008ms
rtt min/avg/max/mdev = 23.568/24.297/25.632/0.823 ms
```

---

➜ **Nommage de la machine**

🌞 **Définissez `node1.tp4.linux` comme nom à la machine**

- montrez moi le contenu du fichier `/etc/hostname`

```bash 
[marielise@node1 ~]$ sudo nano /etc/hostname 
node1.tp4.linux
```

- tapez la commande `hostname` (sans argument ni option) pour afficher votre hostname actuel

```bash
[marielise@node1 ~]$ hostname
node1.tp4.linux
```

# III. Mettre en place un service

## 2. Install

🌞 **Installez NGINX en vous référant à des docs online**

```bash 
[marielise@node1 ~]$ sudo dnf install nginx
[marielise@node1 ~]$ sudo systemctl start nginx
```

## 3. Analyse

🌞 **Analysez le service NGINX**

(j'ai fait la plupart de ces commandes aprés avoir fini le tp donc je ne peux pas te montrer comment c'était avant :/)

- avec une commande `ps`, déterminer sous quel utilisateur tourne le processus du service NGINX
```bash 
[marielise@node1 ~]$ ps aux
titi        1547  0.0  0.9 151820  7896 ?        S    23:18   0:00 nginx: worker
```

- avec une commande `ss`, déterminer derrière quel port écoute actuellement le serveur web
```bash
[marielise@node1 nginx]$ sudo ss -ltpn
State     Recv-Q    Send-Q       Local Address:Port       Peer Address:Port    Process                                                     
LISTEN    0         128                   [::]:80                 [::]:*        users:(("nginx",pid=4927,fd=9),("nginx",pid=4926,fd=9))
```

- inspectez les fichiers de la racine web, et vérifier qu'ils sont bien accessibles en lecture par l'utilisateur qui lance le processus
```bash 
[marielise@node1 ~]$ sudo namei -mo /etc/nginx/nginx.conf
f: /etc/nginx/nginx.conf
 dr-xr-xr-x root root /
 drwxr-xr-x root root etc
 drwxr-xr-x root root nginx
 -rw-r--r-- root root nginx.conf
 ```

## 4. Visite du service web

🌞 **Configurez le firewall pour autoriser le trafic vers le service NGINX** 

```bash 
[marielise@node1 ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
```

```bash 
[marielise@node1 ~]$ sudo firewall-cmd --reload
success
```

🌞 **Tester le bon fonctionnement du service**

```bash
[marielise@node1 ~]$ curl http://10.250.1.56:80
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title>Test Page for the Nginx HTTP Server on Rocky Linux</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style type="text/css">
      /*<![CDATA[*/
      body {
        ...
        <a href="http://www.rockylinux.org/"><img
            src="poweredby.png"
            alt="[ Powered by Rocky Linux ]"
            width="88" height="31" /></a>
        
      </div>
    </div>
  </body>
</html>
```

## 5. Modif de la conf du serveur web

🌞 **Changer le port d'écoute**

- une simple ligne à modifier, vous me la montrerez dans le compte rendu
  - faites écouter NGINX sur le port 8080
  ```bash
    listen       [::]:8080 default_server;
```

- redémarrer le service pour que le changement prenne effet
  ```bash
  [marielise@node1 nginx]$ systemctl status nginx
    nginx.service - The nginx HTTP and reverse proxy server
   Loaded: loaded (/usr/lib/systemd/system/nginx.service; disabled; vendor preset: disabled)
   Active: active (running) since Tue 2021-11-23 18:35:35 CET; 1min 2s ago
   Process: 4982 ExecStart=/usr/sbin/nginx (code=exited, status=0/SUCCESS)
  ```
- prouvez-moi que le changement a pris effet avec une commande `ss`
```bash
[marielise@node1 nginx]$ sudo ss -ltpn
State     Recv-Q    Send-Q       Local Address:Port       Peer Address:Port    Process                                             
LISTEN    0         128                   [::]:8080               [::]:*        users:(("nginx",pid=4985,fd=9),("nginx",pid=4984,fd=9)) 
```
- n'oubliez pas de fermer l'ancier port dans le firewall, et d'ouvrir le nouveau
```bash
[marielise@node1 nginx]$ sudo firewall-cmd --add-port=8080/tcp --permanent
success
[marielise@node1 nginx]$ sudo firewall-cmd --reload
success
```
- prouvez avec une commande `curl` sur votre machine que vous pouvez désormais visiter le port 8080
```bash
[marielise@node1 nginx]$ curl http://10.250.1.56:8080
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
  <head>
    <title>Test Page for the Nginx HTTP Server on Rocky Linux</title>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style type="text/css">
      /*<![CDATA[*/
      body {
        background-color: #fff;
        color: #000;
        font-size: 0.9em;
     ...
    </div>
  </body>
</html>
```

---

🌞 **Changer l'utilisateur qui lance le service**

- vous me montrerez la conf effectuée dans le compte-rendu
  ```bash 
    [marielise@node1 ~]$ sudo nano /etc/nginx/nginx.conf
    user titi;
    worker_processes auto;
    error_log /var/log/nginx/error.log;
    pid /run/nginx.pid;
  ```
- vous prouverez avec une commande `ps` que le service tourne bien sous ce nouveau utilisateur
```bash 
[marielise@node1 ~]$ ps aux
titi        1547  0.0  0.9 151820  7896 ?        S    23:18   0:00 nginx: worker
```

---

🌞 **Changer l'emplacement de la racine Web**

- vous me montrerez la conf effectuée dans le compte-rendu
  ```bash 
    [marielise@node1 ~]$ sudo nano /etc/nginx/nginx.conf
    server {
	listen       8080 default_server;
        listen       [::]:8080 default_server;
        server_name  _;
        root         /var/www/super_site_web;
        index index.html
    ```

- prouvez avec un `curl` depuis votre hôte que vous accédez bien au nouveau site
```bash 
[marielise@node1 ~]$ curl http://10.250.1.56:8080
<h1>toto</h1>
```

