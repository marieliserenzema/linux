# TP6 : Stockage et sauvegarde

# Partie 1 : Préparation de la machine `backup.tp6.linux`

# I. Ajout de disque

🌞 **Ajouter un disque dur de 5Go à la VM `backup.tp6.linux`**
```bash
[marielise@backup ~]$ lsblk
NAME        MAJ:MIN RM  SIZE RO TYPE MOUNTPOINT
sdb           8:16   0    5G  0 disk 
```

# II. Partitioning

🌞 **Partitionner le disque à l'aide de LVM**

- créer un *physical volume (PV)* : le nouveau disque ajouté à la VM
```bash 
[marielise@backup ~]$  sudo pvcreate /dev/sdb
  Physical volume "/dev/sdb" successfully created.
```
- créer un nouveau *volume group (VG)*
```bash 
[marielise@backup ~]$ sudo vgcreate backup /dev/sdb
Volume group "backup" successfully created
```
- créer un nouveau *logical volume (LV)* : ce sera la partition utilisable
```bash
[marielise@backup ~]$ sudo lvcreate -l 100%FREE backup -n last_data
  Logical volume "last_data" created. 
```
🌞 **Formater la partition**

- vous formaterez la partition en ext4 (avec une commande `mkfs`)
```bash
[marielise@backup ~]$ sudo mkfs -t ext4 /dev/backup/last_data
mke2fs 1.45.6 (20-Mar-2020)
En train de créer un système de fichiers avec 1309696 4k blocs et 327680 i-noeuds.
UUID de système de fichiers=ef2cce5e-7a21-4f5d-aa71-dca558541781
Superblocs de secours stockés sur les blocs : 
	32768, 98304, 163840, 229376, 294912, 819200, 884736

Allocation des tables de groupe : complété                        
Écriture des tables d'i-noeuds : complété                        
Création du journal (16384 blocs) : complété
Écriture des superblocs et de l'information de comptabilité du système de
fichiers : complété
```

🌞 **Monter la partition**

- montage de la partition (avec la commande `mount`)
```bash 
[marielise@backup ~]$ sudo  mount /dev/backup/last_data /backup
[marielise@backup ~]$ df -h
Sys. de fichiers             Taille Utilisé Dispo Uti% Monté sur
devtmpfs                       387M       0  387M   0% /dev
tmpfs                          405M       0  405M   0% /dev/shm
tmpfs                          405M    5,6M  400M   2% /run
tmpfs                          405M       0  405M   0% /sys/fs/cgroup
/dev/mapper/rl-root            6,2G    2,2G  4,1G  35% /
/dev/sda1                     1014M    267M  748M  27% /boot
tmpfs                           81M       0   81M   0% /run/user/1000
/dev/mapper/backup-last_data   4,9G     20M  4,6G   1% /backup
```
  - prouvez que vous pouvez lire et écrire des données sur cette partition
 

- définir un montage automatique de la partition (fichier `/etc/fstab`)
  - vous vérifierez que votre fichier `/etc/fstab` fonctionne correctement
```bash 
[marielise@backup ~]$ sudo nano /etc/fstab 
[marielise@backup ~]$ sudo umount /backup 
[marielise@backup ~]$ sudo mount -av
/                         : ignoré
/boot                     : déjà monté
none                      : ignoré
mount : /backup ne contient pas d'étiquettes SELinux.
       Vous avez monté un système de fichiers permettant l'utilisation
       d’étiquettes, mais qui n'en contient pas, sur un système SELinux.
       Les applications sans droit généreront probablement des messages
       AVC et ne pourront pas accéder à ce système de fichiers.
       Pour plus de précisions, consultez restorecon(8) et mount(8).
/backup                  : successfully mounted
[marielise@backup ~]$ sudo reboot
Connection to 10.5.1.13 closed by remote host.
Connection to 10.5.1.13 closed.
```

---

# III. Bonus

➜ Ajouter un deuxième disque de 5Go à la VM et faire une partition de 10Go

- faites en un PV
- ajoutez le au VG existant (il fait donc 10 Go maintenant)
- étendez la partition à 10Go
- prouvez que la partition utilisable fait 10Go désormais



# Partie 2 : Setup du serveur NFS sur `backup.tp6.linux`

🌞 **Préparer les dossiers à partager**

- créez deux sous-dossiers dans l'espace de stockage dédié
```bash 
[marielise@backup ~]$ sudo mkdir /backup/web.tp6.linux/
[marielise@backup ~]$ sudo mkdir /backup/db.tp6.linux/
```

🌞 **Install du serveur NFS**

- installez le paquet `nfs-utils`
```bash 
[marielise@backup ~]$ sudo dnf install nfs-utils
```

🌞 **Conf du serveur NFS**

- fichier `/etc/idmapd.conf`
```bash 
[marielise@backup ~]$ cat /etc/idmapd.conf  | grep Domain
Domain = tp6.linux
```

- fichier `/etc/exports`
```bash
[marielise@backup ~]$ sudo cat /etc/exports
/backup/web.tp6.linux/ 10.5.1.11(rw,no_root_squash)
/backup/db.tp6.linux/ 10.5.1.12(rw,no_root_squash)
```

Les machins entre parenthèses `(rw,no_root_squash)` sont les options de partage. 
rw = droit de lire et écrire les fichiers dans le dossier partagé
no_root_squash spécifie que le root de la machine sur laquelle le répertoire est monté à les droits de root sur le répertoire 

🌞 **Démarrez le service**

- le service s'appelle `nfs-server`
```bash 
[marielise@backup ~]$ sudo systemctl start nfs-server
```
- après l'avoir démarré, prouvez qu'il est actif
```bash
[marielise@backup ~]$ sudo systemctl status nfs-server | grep Active
   Active: active (exited) since Fri 2021-12-03 16:07:42 CET; 30s ago
```
- faites en sorte qu'il démarre automatiquement au démarrage de la machine
```bash 
[marielise@backup ~]$ sudo systemctl enable nfs-server
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
```

🌞 **Firewall**

- le port à ouvrir et le `2049/tcp`
```bash 
[marielise@backup ~]$ sudo firewall-cmd --add-port=2049/tcp --permanent
success
[marielise@backup ~]$ sudo firewall-cmd --reload
success
```
- prouvez que la machine écoute sur ce port (commande `ss`)
```bash 
[marielise@backup ~]$ sudo ss -tunlp | grep 2049
tcp   LISTEN 0      64           0.0.0.0:2049       0.0.0.0:*                                                              
tcp   LISTEN 0      64              [::]:2049          [::]:*   
```

---

# Partie 3 : Setup des clients NFS : `web.tp6.linux` et `db.tp6.linux`

🌞 **Install'**

- le paquet à install pour obtenir un client NFS c'est le même que pour le serveur : `nfs-utils`
```bash 
[marielise@web ~]$ sudo dnf install nfs-utils
```

🌞 **Conf'**

- créez un dossier `/srv/backup` dans lequel sera accessible le dossier partagé
```bash
[marielise@web ~]$ sudo mkdir /srv/backup
```
- pareil que pour le serveur : fichier `/etc/idmapd.conf`
```bash
[marielise@web ~]$ cat /etc/idmapd.conf | grep Domain
Domain = tp6.linux
```

---

🌞 **Montage !**

- montez la partition NFS `/backup/web.tp6.linux/` avec une comande `mount`
```bash
[marielise@web ~]$ sudo mount -t nfs 10.5.1.13:/backup/web.tp6.linux /srv/backup 
```
- preuve avec une commande `df -h` que la partition est bien montée
```bash
[marielise@web ~]$ df -h | grep backup
10.5.1.13:/backup/web.tp6.linux  4.9G   20M  4.6G   1% /srv/backup
```
- prouvez que vous pouvez lire et écrire des données sur cette partition
```bash
[marielise@web ~]$ echo "toto" > /srv/backup/test.txt
[marielise@web ~]$ cat /srv/backup/test.txt 
toto
- définir un montage automatique de la partition (fichier `/etc/fstab`)
```bash 
[marielise@web ~]$ cat /etc/fstab | grep backup
10.5.1.13:/backup/web.tp6.linux /srv/backup nfs defaults 0 0

[marielise@web ~]$ sudo umount /srv/backup
[marielise@web ~]$ sudo mount -av
/srv/backup              : successfully mounted
```

---

🌞 **Répétez les opérations sur `db.tp6.linux`**

```bash
[marielise@db ~]$ sudo dnf install nfs-utils 
Complete!

[marielise@db ~]$ sudo mkdir /srv/backup 
[marielise@db ~]$ cat /etc/idmapd.conf | grep tp6.linux
Domain = tp6.linux

[marielise@db ~]$ sudo mount -t nfs 10.5.1.13:/backup/db.tp6.linux /srv/backup 

[marielise@db ~]$ df -h | grep backup
10.5.1.13:/backup/db.tp6.linux  4.9G   20M  4.6G   1% /srv/backup

[marielise@db ~]$ cat /etc/fstab | grep db
10.5.1.13:/backup/db.tp6.linux /srv/backup nfs defaults 0 0

[marielise@db ~]$ sudo umount /srv/backup 
[marielise@db ~]$ sudo mount -av
/srv/backup              : successfully mounted
```
# Partie 4 : Scripts de sauvegarde


## I. Sauvegarde Web

🌞 **Ecrire un script qui sauvegarde les données de NextCloud**

- le script crée un fichier `.tar.gz` qui contient tout le dossier de NextCloud
- le fichier doit être nommé `nextcloud_yymmdd_hhmmss.tar.gz`
- il doit être stocké dans le répertoire de sauvegarde : `/srv/backup/`
- le script génère une ligne de log à chaque backup effectuée
  - message de log : `[yy/mm/dd hh:mm:ss] Backup /srv/backup/<NAME> created successfully.`
  - fichier de log : `/var/log/backup/backup.log`
- le script affiche une ligne dans le terminal à chaque backup effectuée
  - message affiché : `Backup /srv/backup/<NAME> created successfully.`


🌞 **Créer un service**

- créer un service `backup.service` qui exécute votre script
- ainsi, quand on lance le service avec `sudo systemctl start backup`, une backup est déclenchée

**NB : vous DEVEZ ajoutez la ligne `Type=oneshot` en dessous de la ligne `ExecStart=` dans votre service pour que tout fonctionne correctement avec votre script.**

🌞 **Vérifier que vous êtes capables de restaurer les données**

- en extrayant les données
- et en les remettant à leur place

🌞 **Créer un *timer***

- un *timer* c'est un fichier qui permet d'exécuter un service à intervalles réguliers
- créez un *timer* qui exécute le service `backup` toutes les heures

Pour cela, créer le fichier `/etc/systemd/system/backup.timer`.

> Notez qu'il est dans le même dossier que le service, et qu'il porte le même nom, mais pas la même extension.

Contenu du fichier `/etc/systemd/system/backup.timer` :

```bash
[Unit]
Description=Lance backup.service à intervalles réguliers
Requires=backup.service

[Timer]
Unit=backup.service
OnCalendar=hourly

[Install]
WantedBy=timers.target
```

Activez maintenant le *timer* avec :

```bash
# on indique qu'on a modifié la conf du système
$ sudo systemctl daemon-reload

# démarrage immédiat du timer
$ sudo systemctl start backup.timer

# activation automatique du timer au boot de la machine
$ sudo systemctl enable backup.timer
```

Enfin, on vérifie que le *timer* a été pris en compte, et on affiche l'heure de sa prochaine exécution :

```bash
$ sudo systemctl list-timers
```

## II. Sauvegarde base de données

🌞 **Ecrire un script qui sauvegarde les données de la base de données MariaDB**

```bash
bash#!/bin/bash
# Sauvegarde les données de Nextcloud
# marielise

# Script vars

log_date=$(date +"[%y/%m/%d %H:%M:%S]")
log_dir='/var/log/backup'
log_file="${log_dir}/backup.log"

name_date=$(date +"%y%m%d")
name_time=$(date +"%H%M%S")
file_path='/var/www/nextcloud/html/'
file_name="nextcloud_${name_date}_${name_time}.tar.gz"
save_dir='/srv/backup/'

# Preflight checks
if [[ ! -w "${log_dir}" ]]
then
  echo "Log dir ${log_dir} is not writable."
  exit 1
fi

if [[ ! -w "${save_dir}" ]]
then
  echo "Save dir ${log_dir} is not writable."
  exit 1
fi


#Save Nextcloud data
tar -czf ${save_dir}${file_name} ${file_path} &> /dev/null

# Write in logfile
log_line="${log_date} Backup ${save_dir}${file_name} created successfully."
echo "${log_line}" >> "${log_file}"

#Print
echo "Backup ${save_dir}${file_name} created successfully."
```

---

🌞 **Créer un service**

```bash
bash#!/bin/bash
# Sauvegarde la base de données
# marielise

# Script vars
log_date=$(date +"[%y/%m/%d %H:%M:%S]")
log_dir='/var/log/backup'
log_file="${log_dir}/backup_db.log"

name_date=$(date +"%y%m%d")
name_time=$(date +"%H%M%S")
file_name="nextcloud_db${name_date}_${name_time}.tar.gz"
save_dir='/srv/backup/'
save_file=${save_dir}${file_name}
user=$(cat /.my.cnf | grep "user" | cut -d '=' -f2)
passwd=$(cat /.my.cnf | grep "passwd" | cut -d '=' -f2)

# Preflight checks
if [[ ! -w "${log_dir}" ]]
then
  echo "Log dir ${log_dir} is not writable."
  exit 1
fi

if [[ ! -w "${save_dir}" ]]
then
  echo "Save dir ${log_dir} is not writable."
  exit 1
fi


#Save Nextcloud_db data
mkdir ${save_file}
mysqldump -h 10.5.1.12 -p${passwd} -u${user} -P 3306 nextcloud > dump.sql
mv dump.sql ${save_file}
# Write in logfile
log_line="${log_date} Backup ${save_dir}${file_name} created successfully."
echo "${log_line}" >> "${log_file}"

#Print
echo "Backup ${save_dir}${file_name} created successfully."
```




