# TP5 : P'tit cloud perso

# I. Setup DB


## 1. Install MariaDB


🌞 **Installer MariaDB sur la machine `db.tp5.linux`**

```bash 
[marielise@db ~]$ sudo dnf install mariadb-server
```

🌞 **Le service MariaDB**

- lancez-le avec une commande `systemctl`
```bash
[marielise@db ~]$ sudo systemctl start mariadb
```
- exécutez la commande `sudo systemctl enable mariadb` pour faire en sorte que MariaDB se lance au démarrage de la machine
``` bash 
[marielise@db ~]$ sudo systemctl enable mariadb 
Created symlink /etc/systemd/system/mysql.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/mysqld.service → /usr/lib/systemd/system/mariadb.service.
Created symlink /etc/systemd/system/multi-user.target.wants/mariadb.service → /usr/lib/systemd/system/mariadb.service.
```

- vérifiez qu'il est bien actif avec une commande `systemctl`
```bash 
[marielise@db ~]$ sudo systemctl status mariadb
● mariadb.service - MariaDB 10.3 database server
   Loaded: loaded (/usr/lib/systemd/system/mariadb.service; enabled; vendor preset: disabled)
   Active: active (running) since Thu 2021-11-25 17:14:17 CET; 37s ago
```
- déterminer sur quel port la base de données écoute avec une commande `ss`
```bash 
[marielise@db ~]$ sudo ss -tunlp
Netid   State    Recv-Q   Send-Q     Local Address:Port     Peer Address:Port  Process                                   
tcp     LISTEN   0        80                     *:3306                *:*      users:(("mysqld",pid=4744,fd=21))     
```
- isolez les processus liés au service MariaDB (commande `ps`)
``` bash
[marielise@db ~]$ sudo ps -ef | grep sql
mysql       4744       1  0 17:14 ?        00:00:00 /usr/libexec/mysqld --basedir=/usr
```

🌞 **Firewall**

- pour autoriser les connexions qui viendront de la machine `web.tp5.linux`, il faut conf le firewall
```bash 
[marielise@db ~]$ sudo firewall-cmd --add-port=3306/tcp --permanent
success
[marielise@db ~]$ sudo firewall-cmd --reload
success
```

## 2. Conf MariaDB

🌞 **Configuration élémentaire de la base**

- exécutez la commande `mysql_secure_installation`
``` bash 
[marielise@db ~]$ mysql_secure_installation
Enter current password for root (enter for none): 
ERROR 1045 (28000): Access denied for user 'root'@'localhost' (using password: YES)
Enter current password for root (enter for none): 
ERROR 1045 (28000): Access denied for user 'root'@'localhost' (using password: YES)
Enter current password for root (enter for none): 
OK, successfully used password, moving on...

## ici j'ai mis un mot de passe à root pour plus de sécurité 

Setting the root password ensures that nobody can log into the MariaDB
root user without the proper authorisation.

Set root password? [Y/n] y
New password: 
Re-enter new password: 
Password updated successfully!
Reloading privilege tables..
 ... Success!

Remove anonymous users? [Y/n] y
 ... Success!
## ici on nous demande si on doit être enregistré pour se connecter à MariaDB, j'ai répondu oui afin que l'utilisateur que nous allons créer soit le seul à pouvoir se connceter

Disallow root login remotely? [Y/n] y
 ... Success!
## ici on nous demande si oui ou non on autorise la connexion par réseau public, j'ai répondu que non je ne voulais pas la connexion par réseau public car il y a moins de chances d'avoir de hack en local

Remove test database and access to it? [Y/n] n
 ... skipping.
 ## ici on nous demande si on veut garder l'accessibilité du test databse par tout les utilisateurs, j'ai répondu non car l'enlever à la fin de la configuration 
 ## lorsque tout fonctionnera

Reload privilege tables now? [Y/n] y
## ici il nous demande si on veut actualiser les autorisations maintenant j'ai choisi oui sinon j'aurais oublié plus tard
 
Thanks for using MariaDB!
```
---

🌞 **Préparation de la base en vue de l'utilisation par NextCloud**

```bash 
[marielise@db ~]$ sudo mysql -u root -p
[sudo] Mot de passe de marielise : 
Enter password: 
Welcome to the MariaDB monitor.  Commands end with ; or \g.
Your MariaDB connection id is 18
Server version: 10.3.28-MariaDB MariaDB Server

Copyright (c) 2000, 2018, Oracle, MariaDB Corporation Ab and others.

Type 'help;' or '\h' for help. Type '\c' to clear the current input statement.

MariaDB [(none)]> # Création de la base de donnée qui sera utilisée par NextCloud
MariaDB [(none)]> CREATE DATABASE IF NOT EXISTS nextcloud CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
Query OK, 1 row affected (0.000 sec)

MariaDB [(none)]> 
MariaDB [(none)]> # On donne tous les droits à l'utilisateur nextcloud sur toutes les tables de la base qu'on vient de créer
MariaDB [(none)]> GRANT ALL PRIVILEGES ON nextcloud.* TO 'nextcloud'@'10.5.1.11';
Query OK, 0 rows affected (0.004 sec)

MariaDB [(none)]> 
MariaDB [(none)]> # Actualisation des privilèges
MariaDB [(none)]> FLUSH PRIVILEGES;
Query OK, 0 rows affected (0.000 sec)

MariaDB [(none)]> Bye
```

## 3. Test

🌞 **Installez sur la machine `web.tp5.linux` la commande `mysql`**

- vous utiliserez la commande `dnf provides` pour trouver dans quel paquet se trouve cette commande
```bash 
[marielise@web ~]$ dnf provides mysql
Dernière vérification de l’expiration des métadonnées effectuée il y a 0:01:05 le jeu. 25 nov. 2021 22:53:39 CET.
mysql-8.0.26-1.module+el8.4.0+652+6de068a7.x86_64 : MySQL client programs and shared libraries
Dépôt               : appstream
Correspondances trouvées dans  :
Provide    : mysql = 8.0.26-1.module+el8.4.0+652+6de068a7
```
``` bash
[marielise@web ~]$ sudo dnf instal mysql-8
```

🌞 **Tester la connexion**

- utilisez la commande `mysql` depuis `web.tp5.linux` pour vous connecter à la base qui tourne sur `db.tp5.linux`

```bash
[marielise@web ~]$ sudo mysql -h 10.5.1.12 -u nextcloud -p
```
(j'ai pas mis le -P car il est 3306 par défault)

- effectuez un bête `SHOW TABLES;`
```bash 
  mysql> SHOW TABLES;
  Empty set (0,00 sec)
```
---

# II. Setup Web

## 1. Install Apache

### A. Apache

🌞 **Installer Apache sur la machine `web.tp5.linux`**

```bash 
[marielise@web ~]$ sudo dnf install httpd
```
---

🌞 **Analyse du service Apache**

```bash 
[marielise@web ~]$ sudo systemctl start httpd
[marielise@web ~]$ sudo systemctl enable httpd
Created symlink /etc/systemd/system/multi-user.target.wants/httpd.service → /usr/lib/systemd/system/httpd.service.
[marielise@web ~]$ sudo ss -tunlp | grep http
tcp   LISTEN 0      128                *:80              *:*    users:(("httpd",pid=6072,fd=4),("httpd",pid=6071,fd=4),("httpd",pid=6070,fd=4),("httpd",pid=6068,fd=4))
[marielise@web ~]$ sudo ps -ef | grep http
root        6068       1  0 23:32 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      6069    6068  0 23:32 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      6070    6068  0 23:32 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      6071    6068  0 23:32 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
apache      6072    6068  0 23:32 ?        00:00:00 /usr/sbin/httpd -DFOREGROUND
marieli+    6311    1563  0 23:35 pts/0    00:00:00 grep --color=auto http
```
---

🌞 **Un premier test**

- ouvrez le port d'Apache dans le firewall
```bash [marielise@web ~]$ sudo firewall-cmd --add-port=80/tcp --permanent
success
[marielise@web ~]$ sudo firewall-cmd --reload
success
```
- testez, depuis votre PC, que vous pouvez accéder à la page d'accueil par défaut d'Apache

``` bash 
    [marielise@web ~]$ curl http://10.5.1.11:80
````


### B. PHP

🌞 **Installer PHP**

```bash
[marielise@web ~]$ sudo dnf install epel-release
[marielise@web ~]$ sudo dnf update
[marielise@web ~]$ sudo dnf module enable php:remi-7.4
[marielise@web ~]$ sudo dnf install zip unzip libxml2 openssl php74-php php74-php-ctype php74-php-curl php74-php-gd php74-php-iconv php74-php-json php74-php-libxml php74-php-mbstring php74-php-openssl php74-php-posix php74-php-session php74-php-xml php74-php-zip php74-php-zlib php74-php-pdo php74-php-mysqlnd php74-php-intl php74-php-bcmath php74-php-gmp
```

## 2. Conf Apache


🌞 **Analyser la conf Apache**

- mettez en évidence, dans le fichier de conf principal d'Apache, la ligne qui inclut tout ce qu'il y a dans le dossier de *drop-in*
```bash 
[marielise@web conf]$ sudo nano httpd.conf
# Load config files in the "/etc/httpd/conf.d" directory, if any.
IncludeOptional conf.d/*.conf
```

🌞 **Créer un VirtualHost qui accueillera NextCloud**



```apache
[marielise@web conf.d]$ cat virtualhost.conf 
<VirtualHost *:80>
  # on précise ici le dossier qui contiendra le site : la racine Web
  DocumentRoot /var/www/nextcloud/html/  

  # ici le nom qui sera utilisé pour accéder à l'application
  ServerName  web.tp5.linux  

  <Directory /var/www/nextcloud/html/>
    Require all granted
    AllowOverride All
    Options FollowSymLinks MultiViews

    <IfModule mod_dav.c>
      Dav off
    </IfModule>
  </Directory>
</VirtualHost>
[marielise@web conf.d]$ sudo systemctl restart httpd
```

🌞 **Configurer la racine web**

- la racine Web, on l'a configurée dans Apache pour être le dossier `/var/www/nextcloud/html/`
- creéz ce dossier
- faites appartenir le dossier et son contenu à l'utilisateur qui lance Apache (commande `chown`, voir le [mémo commandes](../../cours/memos/commandes.md))
```apache 
[marielise@web ~]$ cd /var/www/
[marielise@web www]$ sudo mkdir nextcloud
[marielise@web www]$ cd nextcloud/
[marielise@web nextcloud]$ sudo mkdir html
[marielise@web nextcloud]$ cd 
[marielise@web ~]$ cd /var/www/
[marielise@web www]$ sudo chown apache nextcloud
[marielise@web www]$ cd nextcloud/
[marielise@web nextcloud]$ sudo chown apache html
```

🌞 **Configurer PHP**

- dans l'install de NextCloud, PHP a besoin de conaître votre timezone (fuseau horaire)
- pour récupérer la timezone actuelle de la machine, utilisez la commande `timedatectl` (sans argument)
- modifiez le fichier `/etc/opt/remi/php74/php.ini` :
  - changez la ligne `;date.timezone =`
  - par `date.timezone = "<VOTRE_TIMEZONE>"`
  - par exemple `date.timezone = "Europe/Paris"`

  ```apache 
  [marielise@web ~]$ sudo nano /etc/opt/remi/php74/php.ini
  date.timezone = "Europe/Paris"
  ```

## 3. Install NextCloud

On dit "installer NextCloud" mais en fait c'est juste récupérer les fichiers PHP, HTML, JS, etc... qui constituent NextCloud, et les mettre dans le dossier de la racine web.

🌞 **Récupérer Nextcloud**

```bash
[marielise@web ~]$ curl -SLO https://download.nextcloud.com/server/releases/nextcloud-21.0.1.zip
[marielise@web ~]$ ls
nextcloud-21.0.1.zip
```

🌞 **Ranger la chambre**

- extraire le contenu de NextCloud (beh ui on a récup un `.zip`)
```bash 
[marielise@web ~]$ unzip nextcloud-21.0.1.zip 
```
- déplacer tout le contenu dans la racine Web
  - n'oubliez pas de gérer les permissions de tous les fichiers déplacés ;)
- supprimer l'archive
```bash 
[marielise@web ~]$ sudo cp -r nextcloud/* /var/www/nextcloud/html/
[marielise@web ~]$ sudo rm -r nextcloud/
[marielise@web ~]$ sudo rm -r nextcloud-21.0.1.zip 
[marielise@web nextcloud]$ sudo chown -R apache html
```

## 4. Test

🌞 **Modifiez le fichier `hosts` de votre PC**

- ajoutez la ligne : `10.5.1.11 web.tp5.linux`
```bash 
MacBook-Pro-de-saperlipopette:etc marieliserenzema$ sudo nano hosts
10.5.1.11 web.tp5.linux
```

🌞 **Tester l'accès à NextCloud et finaliser son install'**

- ouvrez votre navigateur Web sur votre PC
- rdv à l'URL `http://web.tp5.linux`
- vous devriez avoir la page d'accueil de NextCloud
- ici deux choses :
  - les deux champs en haut pour créer un user admin au sein de NextCloud
  - le bouton "Configure the database" en bas
    - sélectionnez "MySQL/MariaDB"
    - entrez les infos pour que NextCloud sache comment se connecter à votre serveur de base de données
    - c'est les infos avec lesquelles vous avez validé à la main le bon fonctionnement de MariaDB (c'était avec la commande `mysql`)
```bash 
[marielise@web ~]$ curl http://web.tp5.linux
<!DOCTYPE HTML PUBLIC "-//IETF//DTD HTML 2.0//EN">
<html><head>
<title>301 Moved Permanently</title>
</head><body>
<h1>Moved Permanently</h1>
<p>The document has moved <a href="http://www.ynov.com/">here</a>.</p>
</body></html>
```
---


