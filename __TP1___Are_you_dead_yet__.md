# TP1 : Are you dead yet ?

Voici les différentes commandes que j'ai utilisé pour casser ma machine virtuelle

---

**Commande 1 : SleepMode**

`cd /home/toto
ls -a 
nano.bashhrc
sleep500000000000000000000000000000`

Cette suite commande bloque le terminal car aprés cette suite de commande, l'utilisateur devra attendre le temps indiqué aprés sleep pour réutiliser le terminal

**Commande 2 : Le Stockage**

`sudo dd if=/dev/zero of = zeros bs 1M`

Cette commande créer un fichier trés lourd qui rempli le disque dur 

**Commande 3 : Supprimer le Root**

`sudo rm -rf / -no-preserve-root`

Cette commande supprime tous les fichiers présents y compris le root, alors la machine vituelle plante complétement

**Commande 4 : Supprimer les fichiers**

`sudo amount -l -f /`

Cette commande supprime toute l'arborescence de la racine, ce qui rend la machine inutilisable 

**Commande 5 : La bombe**

`:(){ :|: & };:`
Cette commande engendre une répétition indéfinie d'une ligne, ce qui va conduire à une surcharge du systéme et provoquer l'inutilisation de la machine

**Commande 6 : L'extinction de l'ordinateur**

Tout simplement, si on éteint l'ordinateur, alors la machine virtuelle devient logiquement inaccessible et inutilisable

**Commande 7 : La suppression du matériel**

Encore plus simplement, si on enlève la souris, le clavier et l'écran de l'ordinateur alors la machine virtuelle sera inutilisable

