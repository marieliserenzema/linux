# TP 3 : A little script

# I. Script carte d'identité

📁 **Fichier `/srv/idcard/idcard.sh`**

```bash 
PCNAME=$(hostname)
echo "Machine name: $PCNAME"
OSname=$(cat /etc/os-release | head -1 | cut -d '"' -f2)
VerKer=$(uname -r)
echo "OS $OSname and kernel version is $VerKer"
IP=$(hostname -I | cut -d ' ' -f2)
echo "IP : $IP"
MemTot=$(free -mh | grep Mem | cut -d ' ' -f11 | cut -b -3 )
MemAvi=$(free -mh | grep Mem | cut -d ' ' -f46 | cut -b -3 )
echo "RAM : $MemAvi Go/$MemTot Go"
Disk=$(df -h | grep /dev/sda5 | cut -d ' ' -f12 | cut -b -3 )
echo "Disque : $Disk Go left"
process1=$(ps -eo %mem,pid,command | sort | tail -6 | head -1)
process2=$(ps -eo %mem,pid,command | sort | tail -5 | head -1)
process3=$(ps -eo %mem,pid,command | sort | tail -4 | head -1)
process4=$(ps -eo %mem,pid,command | sort | tail -3 | head -1)
process5=$(ps -eo %mem,pid,command | sort | tail -2 | head -1)
echo "Top 5 process by Ram usage :"
echo " - $process1"
echo " - $process2"
echo " - $process3"
echo " - $process4"
echo " - $process5"
echo "Listening ports :"
ss -alnpt -H | while read line
do 
port=$(echo $line | tr -s ' ' | cut -d ' ' -f4 | rev | cut -d ':'  -f1| rev)
nom=$(echo $line | cut -d '"' -f2)
echo " - ${port} : ${nom} "
done
chat=$(curl -s https://api.thecatapi.com/v1/images/search| cut -d '"' -f10)
echo "Voici ton image de chat : $chat"
```

🌞 Vous fournirez dans le compte-rendu, en plus du fichier, **un exemple d'exécution avec une sortie**, dans des balises de code.

```bash 
marie@node1:/srv/idcard$ sudo bash idcard.sh
Machine name : node1.tp2.linux
OS is Ubuntu 21.10 and kernel version is 5.13.0-21-generic
Device "enp0s9" does not exist.
Device "enp0s9" does not exist.
IP : 
RAM : 421Mi/971Mi
Disque : 2,3G space left
Top 5 processes by RAM usage :
  - xfwm4 (ID 1133) : 7.9%
  - /usr/lib/xorg/Xorg (ID 591) : 7.5%
  - /usr/bin/python3 (ID 1223) : 4.9%
  - Thunar (ID 1156) : 4.7%
  - /usr/lib/x86_64-linux-gnu/xfce4/panel/wrapper-2.0 (ID 1169) : 4.2%
Listening ports :
  - 53 : systemd-resolve
  - 22 : sshd
  - 631 : cupsd
  - 2121 : vsftpd
  - 22 : sshd
  - 631 : cupsd
Here's your random cat : https://cdn2.thecatapi.com/images/d6v.jpg
```

# II. Script youtube-dl

📁 **Le script `/srv/yt/yt.sh`**
``` bash 
FILE=/srv/yt/downloads
if [ -d "$FILE" ]; then
    echo "$FILE exists."
else 
    echo "$FILE does not exist."
    exit
fi
youtube_video_url="$1"
nom_video=$(youtube-dl --get-title $youtube_video_url)
youtube-dl -o "/srv/yt/downloads/$nom_video/%(title)s.%(ext)s" $youtube_video_url > /dev/null
youtube-dl --get-description $youtube_video_url >> "/srv/yt/downloads/$nom_video/description"

echo "Video $youtube_video_url has been sucessfuly downloaded."
echo "File path : /srv/yt/downloads/$nom_video/$nom_video.mp4"
echo "[$(date '+%D %T')] Video $youtube_video_url was downloaded. File path : /srv/yt/downloads/$nom_video/$nom_vidoe.mp4" >> "/var/log/yt/download.log"
```

📁 **Le fichier de log `/var/log/yt/download.log`**, avec au moins quelques lignes
```bash
marie@node1:/var/log/yt$ cat download.log
[11/22/21 14:39:35] Video  was downloaded. File path : /srv/yt/downloads//.mp4
[11/22/21 14:41:22] Video https://www.youtube.com/watch?v=EU4nktO6xpk was downloaded. File path : /srv/yt/downloads/Vettel: "I'm gonna touch Hamilton's rear wing"/.mp4
[11/22/21 14:49:15] Video https://www.youtube.com/watch?v=_4Tmqj-uXQs was downloaded. File path : /srv/yt/downloads/Formula 1 In 10 Seconds/.mp4
[11/22/21 14:52:18] Video https://www.youtube.com/watch?v=GWCITkGdki8 was downloaded. File path : /srv/yt/downloads/MAX VERSTAPPEN REAR WING VIBRATION/.mp4
[11/22/21 22:15:36] Video https://www.youtube.com/watch?v=tbnLqRW9Ef0 was downloaded. File path : /srv/yt/downloads/1 sec VIDEO/.mp4
```

🌞 Vous fournirez dans le compte-rendu, en plus du fichier, **un exemple d'exécution avec une sortie**, dans des balises de code.
```bash
marie@node1:/srv/yt$ sudo bash yt.sh https://www.youtube.com/watch?v=tbnLqRW9Ef0
/srv/yt/downloads exists.
Video https://www.youtube.com/watch?v=tbnLqRW9Ef0 has been sucessfuly downloaded.
File path : /srv/yt/downloads/1 sec VIDEO/1 sec VIDEO.mp4
```

# III. MAKE IT A SERVICE

## Rendu

📁 **Le script `/srv/yt/yt-v2.sh`**

```bash 
while true; do
        dl_file="$(cat /srv/yt/to_dl)"
        if [[ (-d "/srv/yt/downloads") && (-d "/var/log/yt") && (-n "/srv/yt/downloads") ]]; then
		cat /srv/yt/to_dl | while read line; do 
			url=$line
			vid_name=$(youtube-dl --get-title $url)
	                path="/srv/yt/downloads/$vid_name"
			mkdir "$path"
	                mkdir "$path/description"
	                echo "Video $url has been downloaded."
	                echo "File path : $path.mp4"
	                youtube-dl -o "$path/%(vid_name)s.%(ext)s" "$url" > "/dev/null"
	                youtube-dl -o "$path/description/$vid_name" --write-description --skip-download --youtube-skip-dash-manifest "$url" > "/dev/null"
	                echo "[$(date "+%Y/%m/%d %T")] Video $url has been succesfully downloaded. File path : $path/$vid_name.mp4" >> "/var/log/yt/download.log"
	                sed -i '1d' "/srv/yt/to_dl"
		done
	fi
	sleep 10
done

```

📁 **Fichier `/etc/systemd/system/yt.service`**

```bash 
marie@node1:/etc/systemd/system$ sudo nano yt.service 
[Unit]
Description=descrption

[Service]
ExecStart=yt-v2.sh

[Install]
WantedBy=multi-user.target
```

🌞 Vous fournirez dans le compte-rendu, en plus des fichiers :

- un `systemctl status yt` quand le service est en cours de fonctionnement
```bash 
marie@node1:~$ sudo systemctl status yt
× yt.service - descrption
     Loaded: loaded (/etc/systemd/system/yt.service; disabled; vendor preset: e>
     Active: failed (Result: exit-code) since Wed 2021-12-01 15:43:40 CET; 5s a>
```

- la commande qui permet à ce service de démarrer automatiquement quand la machine démarre
```bash
marie@node1:~$ systemctl enable yt.service
==== AUTHENTICATING FOR org.freedesktop.systemd1.manage-unit-files ===
Authentication is required to manage system service or unit files.
Authenticating as: Marie Lise,,, (marie)
Password: 
==== AUTHENTICATION COMPLETE ===
Created symlink /etc/systemd/system/multi-user.target.wants/yt.service → /etc/systemd/system/yt.service.
==== AUTHENTICATING FOR org.freedesktop.systemd1.reload-daemon ===
Authentication is required to reload the systemd state.
Authenticating as: Marie Lise,,, (marie)
Password: 
==== AUTHENTICATION COMPLETE ===
```
