# Rendu des TP de Linux

Ce dépôt Git est destiné à rendre les travaux pratiques du cours de Linux lors de ma première année d'informatique à Ynov Bordeaux.

## Structure du dépôt

Le dépôt est organisé en fichiers correspondant à chaque TP, numérotés dans l'ordre chronologique.