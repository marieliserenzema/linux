# TP2 : Explorer et manipuler le système


## 1. Une machine xubuntu fonctionnelle

## 2. Nommer la machine

🌞 **Changer le nom de la machine**

`sudo hostname <NOM_MACHINE>`
`sudo nano /etc/hostname`
`sudo cat /etc/hostname`
 
## 3. Config réseau

➜ **Vérifiez avant de continuer le TP que la configuration réseau de la machine est OK. C'est à dire :**

- la machine doit pouvoir joindre internet
- votre PC doit pouvoir `ping` la machine

Pour vérifier que vous avez une configuration réseau correcte (étapes à réaliser DANS LA VM) :

```bash
# Affichez la liste des cartes réseau de la machine virtuelle
# Vérifiez que les cartes réseau ont toute une IP
$ ip a

# Si les cartes n'ont pas d'IP vous pouvez les allumez avec la commande
$ nmcli con up <NOM_INTERFACE>
# Par exemple
$ nmcli con up enp0s3

# Vous devez repérer l'adresse de la VM dans le host-only

# Vous pouvez tester de ping un serveur connu sur internet
# On teste souvent avec 1.1.1.1 (serveur DNS de CloudFlare)
# ou 8.8.8.8 (serveur DNS de Google)
$ ping 1.1.1.1

# On teste si la machine sait résoudre des noms de domaine
$ ping ynov.com
```

Ensuite on vérifie que notre PC peut `ping` la machine :

```bash
# Afficher la liste de vos carte réseau, la commande dépend de votre OS
$ ip a # Linux
$ ipconfig # Windows
$ ifconfig # MacOS

# Vous devez repérer l'adresse de votre PC dans le host-only

# Ping de l'adresse de la VM dans le host-only
$ ping <IP_VM>
```

🌞 **Config réseau fonctionnelle**

- dans le compte-rendu, vous devez me montrer...
  - depuis la VM : `ping 1.1.1.1` fonctionnel
  - depuis la VM : `ping ynov.com` fonctionnel
    - depuis votre PC : `ping <IP_VM>` fonctionnel
```bash
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ ping 192.168.57.5
PING 192.168.57.5 (192.168.57.5): 56 data bytes
64 bytes from 192.168.57.5: icmp_seq=0 ttl=64 time=0.639 ms
64 bytes from 192.168.57.5: icmp_seq=1 ttl=64 time=0.779 ms
64 bytes from 192.168.57.5: icmp_seq=2 ttl=64 time=0.520 ms
64 bytes from 192.168.57.5: icmp_seq=3 ttl=64 time=0.456 ms
^C
--- 192.168.57.5 ping statistics ---
4 packets transmitted, 4 packets received, 0.0% packet loss
round-trip min/avg/max/stddev = 0.456/0.599/0.779/0.123 ms
```

# Partie 1 : SSH


# II. Setup du serveur SSH


## 1. Installation du serveur


🌞 **Installer le paquet `openssh-server`**

```bash
marie@node1:~$ sudo apt install openssh-server
[sudo] password for marie: 
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
openssh-server is already the newest version (1:8.4p1-6ubuntu2).
0 upgraded, 0 newly installed, 0 to remove and 6 not upgraded.
```


## 2. Lancement du service SSH

🌞 **Lancer le service `ssh`**

```bash
marie@node1:~$ sudo systemctl start ssh
```

- vérifier que le service est actuellement actif 

```bash
marie@node1:~$ sudo systemctl status ssh
● ssh.service - OpenBSD Secure Shell server
     Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2021-10-25 16:51:21 CEST; 15min ago
       Docs: man:sshd(8)
             man:sshd_config(5)
    Process: 1854 ExecStartPre=/usr/sbin/sshd -t (code=exited, status=0/SUCCESS)
   Main PID: 1855 (sshd)
      Tasks: 1 (limit: 1106)
     Memory: 2.9M
        CPU: 46ms
     CGroup: /system.slice/ssh.service
             └─1855 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups

oct. 25 16:51:21 node1.tp2.linux systemd[1]: Starting OpenBSD Secure Shell server...
oct. 25 16:51:21 node1.tp2.linux sshd[1855]: Server listening on 0.0.0.0 port 22.
oct. 25 16:51:21 node1.tp2.linux sshd[1855]: Server listening on :: port 22.
oct. 25 16:51:21 node1.tp2.linux systemd[1]: Started OpenBSD Secure Shell server.
oct. 25 17:04:44 node1.tp2.linux sshd[2887]: Accepted password for marie from 192.168.57.1 port 64490 ssh2
oct. 25 17:04:44 node1.tp2.linux sshd[2887]: pam_unix(sshd:session): session opened for user marie by (uid=0)
```


## 3. Etude du service SSH

🌞 **Analyser le service en cours de fonctionnement**


- afficher le statut du *service*
  ```bash
marie@node1:~$ sudo systemctl status ssh
● ssh.service - OpenBSD Secure Shell server
     Loaded: loaded (/lib/systemd/system/ssh.service; enabled; vendor preset: enabled)
     Active: active (running) since Mon 2021-10-25 16:51:21 CEST; 15min ago
       Docs: man:sshd(8)
             man:sshd_config(5)
    Process: 1854 ExecStartPre=/usr/sbin/sshd -t (code=exited, status=0/SUCCESS)
   Main PID: 1855 (sshd)
      Tasks: 1 (limit: 1106)
     Memory: 2.9M
        CPU: 46ms
     CGroup: /system.slice/ssh.service
             └─1855 sshd: /usr/sbin/sshd -D [listener] 0 of 10-100 startups

oct. 25 16:51:21 node1.tp2.linux systemd[1]: Starting OpenBSD Secure Shell server...
oct. 25 16:51:21 node1.tp2.linux sshd[1855]: Server listening on 0.0.0.0 port 22.
oct. 25 16:51:21 node1.tp2.linux sshd[1855]: Server listening on :: port 22.
oct. 25 16:51:21 node1.tp2.linux systemd[1]: Started OpenBSD Secure Shell server.
oct. 25 17:04:44 node1.tp2.linux sshd[2887]: Accepted password for marie from 192.168.57.1 port 64490 ssh2
oct. 25 17:04:44 node1.tp2.linux sshd[2887]: pam_unix(sshd:session): session opened for user marie by (uid=0)
```
- afficher le/les processus liés au *service* `ssh`
  ```bash 
  marie@node1:~$ ps -e
    PID TTY          TIME CMD
   2887 ?        00:00:00 sshd
   2965 ?        00:00:00 sshd
   9099 ?        00:00:00 sshd
   9109 ?        00:00:00 sshd
   9191 ?        00:00:00 sshd
  ```
- afficher le port utilisé par le *service* `ssh`
  ```bash
  marie@node1:~$ ss -l
    Netid      State       Recv-Q      Send-Q                                        Local Address:Port                           Peer Address:Port       Process                           
    tcp        LISTEN      0           128                                                    [::]:ssh                            [::]:*  
    ```              

- afficher les logs du *service* `ssh`
  ```bash
    marie@node1:/var/log$ journalctl -xe -u ssh
    -- Journal begins at Mon 2021-10-25 16:01:08 CEST, ends at Mon 2021-10-25 17:21:24 CEST. --
    ```

---

🌞 **Connectez vous au serveur**

- depuis votre PC, en utilisant un **client SSH**

```bash
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ ssh marie@192.168.57.5
The authenticity of host '192.168.57.5 (192.168.57.5)' can't be established.
ECDSA key fingerprint is SHA256:kH0oM6BA5crIJ+cBGoLQVsY3ad9L47Rsc0DWvU+sWqM.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '192.168.57.5' (ECDSA) to the list of known hosts.
marie@192.168.57.5's password: 
Welcome to Ubuntu 21.10 (GNU/Linux 5.13.0-20-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

6 updates can be applied immediately.
6 of these updates are standard security updates.
To see these additional updates run: apt list --upgradable


The programs included with the Ubuntu system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Ubuntu comes with ABSOLUTELY NO WARRANTY, to the extent permitted by
applicable law.
```

## 4. Modification de la configuration du serveur

🌞 **Modifier le comportement du service**

```bash 
marie@node1:~$ cat /etc/ssh/sshd_config
#	$OpenBSD: sshd_config,v 1.103 2018/04/09 20:41:22 tj Exp $
#Port 1026
#AddressFamily any
```
- pour cette modification, prouver à l'aide d'une commande qu'elle a bien pris effet
  - une commande `ss -l` pour vérifier le port d'écoute

```bash
marie@node1:~$ sudo ss -tunlp
Netid          State           Recv-Q          Send-Q                   Local Address:Port                    Peer Address:Port Process                                                     
tcp            LISTEN          0               128                               [::]:1026                            [::]:*             users:(("sshd",pid=9099,fd=4))  
```

🌞 **Connectez vous sur le nouveau port choisi**

- depuis votre PC, avec un *client SSH*

```bash
MacBook-Pro-de-saperlipopette:~ marieliserenzema$ ssh marie@192.168.57.5 -p 1026
marie@192.168.57.5's password: 
Welcome to Ubuntu 21.10 (GNU/Linux 5.13.0-20-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage

44 updates can be applied immediately.
To see these additional updates run: apt list --upgradable

Last login: Tue Oct 26 03:27:44 2021 from 192.168.57.1
marie@node1:~$ 
```

# Partie 2 : FTP

# II. Setup du serveur FTP


## 1. Installation du serveur

🌞 **Installer le paquet `vsftpd`**

```bash 
marie@node1:~$ sudo apt-get install vsftpd
[sudo] password for marie: 
Reading package lists... Done
Building dependency tree... Done
Reading state information... Done
The following NEW packages will be installed:
  vsftpd
0 upgraded, 1 newly installed, 0 to remove and 44 not upgraded.
Need to get 122 kB of archives.
After this operation, 322 kB of additional disk space will be used.
Get:1 http://fr.archive.ubuntu.com/ubuntu impish/main amd64 vsftpd amd64 3.0.3-13build1 [122 kB]
Fetched 122 kB in 0s (461 kB/s)
Preconfiguring packages ...
Selecting previously unselected package vsftpd.
(Reading database ... 195388 files and directories currently installed.)
Preparing to unpack .../vsftpd_3.0.3-13build1_amd64.deb ...
Unpacking vsftpd (3.0.3-13build1) ...
Setting up vsftpd (3.0.3-13build1) ...
Created symlink /etc/systemd/system/multi-user.target.wants/vsftpd.service → /lib/systemd/system/vsftpd.service.
Processing triggers for man-db (2.9.4-2) ...
```

## 2. Lancement du service FTP

🌞 **Lancer le service `vsftpd`**

```bash
marie@node1:~$ sudo systemctl start vsftpd
marie@node1:~$ sudo systemctl status vsftpd
● vsftpd.service - vsftpd FTP server
     Loaded: loaded (/lib/systemd/system/vsftpd.service; enabled; vendor preset: enabled)
     Active: active (running) since Thu 2021-11-04 14:41:03 CET; 2min 44s ago
    Process: 9357 ExecStartPre=/bin/mkdir -p /var/run/vsftpd/empty (code=exited, status=0/SUCCESS)
   Main PID: 9358 (vsftpd)
      Tasks: 1 (limit: 1106)
     Memory: 700.0K
        CPU: 4ms
     CGroup: /system.slice/vsftpd.service
             └─9358 /usr/sbin/vsftpd /etc/vsftpd.conf

nov. 04 14:41:03 node1.tp2.linux systemd[1]: Starting vsftpd FTP server...
nov. 04 14:41:03 node1.tp2.linux systemd[1]: Started vsftpd FTP server.
```

## 3. Etude du service FTP

🌞 **Analyser le service en cours de fonctionnement**

- afficher le statut du service
  ```bash
  marie@node1:~$ sudo systemctl status vsftpd
● vsftpd.service - vsftpd FTP server
     Loaded: loaded (/lib/systemd/system/vsftpd.service; enabled; vendor preset: enabled)
     Active: active (running) since Thu 2021-11-04 14:41:03 CET; 2min 44s ago
    Process: 9357 ExecStartPre=/bin/mkdir -p /var/run/vsftpd/empty (code=exited, status=0/SUCCESS)
   Main PID: 9358 (vsftpd)
      Tasks: 1 (limit: 1106)
     Memory: 700.0K
        CPU: 4ms
     CGroup: /system.slice/vsftpd.service
             └─9358 /usr/sbin/vsftpd /etc/vsftpd.conf

nov. 04 14:41:03 node1.tp2.linux systemd[1]: Starting vsftpd FTP server...
nov. 04 14:41:03 node1.tp2.linux systemd[1]: Started vsftpd FTP server.
```
- afficher le/les processus liés au service `vsftpd`
  ```bash
  marie@node1:~$ ps -e
    PID TTY          TIME CMD
   9358 ?        00:00:00 vsftpd
  ```

- afficher le port utilisé par le service `vsftpd`
  - avec une commande `ss -l`
  - isolez uniquement la/les ligne(s) intéressante(s)
- afficher les logs du service `vsftpd`
  - avec une commande `journalctl`
  - en consultant un fichier dans `/var/log/`
  - ne me donnez pas toutes les lignes de logs, je veux simplement que vous appreniez à consulter les logs

---

🌞 **Connectez vous au serveur**

- depuis votre PC, en utilisant un *client FTP*
  - les navigateurs Web, ils font ça maintenant
  - demandez moi si vous êtes perdus
- essayez d'uploader et de télécharger un fichier
  - montrez moi à l'aide d'une commande la ligne de log pour l'upload, et la ligne de log pour le download
- vérifier que l'upload fonctionne
  - une fois un fichier upload, vérifiez avec un `ls` sur la machine Linux que le fichier a bien été uploadé

🌞 **Visualiser les logs**

- mettez en évidence une ligne de log pour un download
- mettez en évidence une ligne de log pour un upload

## 4. Modification de la configuration du serveur

Pour modifier comment un service se comporte il faut modifier de configuration. On peut tout changer à notre guise.

🌞 **Modifier le comportement du service**

- c'est dans le fichier `/etc/vsftpd.conf`
- effectuez les modifications suivantes :
  - changer le port où écoute `vstfpd`
    - peu importe lequel, il doit être compris entre 1025 et 65536
  - vous me prouverez avec un `cat` que vous avez bien modifié cette ligne
- pour les deux modifications, prouver à l'aide d'une commande qu'elles ont bien pris effet
  - une commande `ss -l` pour vérifier le port d'écoute

> Vous devez redémarrer le service avec une commande `systemctl restart` pour que les changements inscrits dans le fichier de configuration prennent effet.

🌞 **Connectez vous sur le nouveau port choisi**

- depuis votre PC, avec un *client FTP*
- re-tester l'upload et le download

![Suuuuuuuuuuuuuure](./pics/files-on-ftp-suuuure-any-minute-now.jpg)
